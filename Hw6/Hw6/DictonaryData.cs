﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Hw6
{
   

    //Different Data from the API
    public partial class Definitions
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("definition")]
        public string Definition { get; set; }

        [JsonProperty("example")]
        public string Example { get; set; }

        [JsonProperty("image_url")]
        public Uri Image_url { get; set; }
        [JsonProperty("emoji")]
        public string Emoji { get; set; }

        [JsonProperty("pronunciation")]
        public string Pronunciation { get; set; }
    }
    //converting fromjson to c#
    public partial class Definitions
    {
        public static Definitions[] FromJson(string json) => JsonConvert.DeserializeObject<Definitions[]>(json, Hw6.Converter.Settings);
    }
    //serializing the data.
    public static class Serialize
    {
        public static string ToJson(this Definitions self) => JsonConvert.SerializeObject(self, Hw6.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
