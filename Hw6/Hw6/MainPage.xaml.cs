﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Hw6
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        

        public MainPage()
        {
            InitializeComponent();
            //Accessing the network
            var current = Connectivity.NetworkAccess;
            //If there is internete connection
            if (current == NetworkAccess.Internet)
            {
                // Saving the profile of the connection 
                var profiles = Connectivity.ConnectionProfiles;
                //if wifi goes here
                if (profiles.Contains(ConnectionProfile.WiFi))
                {
                    DisplayAlert("Alert", "Phone is connected with wifi", "OK");
                }
                //if mobile data goes here.
                else
                {
                    DisplayAlert("Alert", "Phone is connected with Mobile Data", "OK");
                }
            }
            ///if no interenetr connection goes here.
            else
            {
                DisplayAlert("Alert", "No Internet Conenction", "OK");
            }
        }
      
        //when get meaning button clicked.
        async void OnGetWordButtonClicked(object sender, EventArgs e)
        {
            //try block to handle all errors.
            try
            {
                //this will make sure we have the data and no null data is sent
                if (!string.IsNullOrWhiteSpace(_wordEntry.Text))
                {
                    // htto client for http uri access
                    HttpClient client = new HttpClient();
                    //saving the word entry
                    string s = _wordEntry.Text;
                    //merging uri and word in one link
                    var uri = new Uri(string.Format($"https://owlbot.info/api/v2/dictionary/" + s));
                   // requesting to get the data
                    var request = new HttpRequestMessage();
                    request.Method = HttpMethod.Get;
                    request.RequestUri = uri;
                    //saving the response of the request.
                    HttpResponseMessage response = await client.SendAsync(request);

                    // DictonaryData dictonaryData = await _dicService.GetDictonaryData(uri);
                    //accessing the data from the class with it's object.
                    Definitions[] dictonaryData = null;
                    if (response.IsSuccessStatusCode)
                    {
                        var content = await response.Content.ReadAsStringAsync();
                        dictonaryData = Definitions.FromJson(content);
                        type1.Text = $"The type of the word is :- {dictonaryData[0].Type}";
                        definition1.Text = $"The definition of the word is :- {dictonaryData[0].Definition}";
                        example1.Text = $"The example of the word is :- {dictonaryData[0].Example}";
                        word1.Text = $"The word is :-" + s;


                    }
                }
            }
            catch(Exception ex)
            {
                //handling the errors
                await DisplayAlert("Alert", ex.Message, "OK");
                
            }

            
        }
    }
}
